set(HEADERS
  dolfin_function.h
  Constant.h
  Function.h
  FunctionSpace.h
  PARENT_SCOPE)

set(SOURCES
  Constant.cpp
  Function.cpp
  FunctionSpace.cpp
  PARENT_SCOPE)
